---
layout: handbook-page-toc
title: "Partner Support"
---
 
 
## On this page
{:.no_toc .hidden-md .hidden-lg}
 
- TOC
{:toc .hidden-md .hidden-lg}
<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />
{::options parse_block_html="true" /}




# Welcome to the Partner Support page
This page is intended for Sales and Channel team members to review quick links and common questions that come to the Partner Help Desk team.

## Meet the Team
### Who We Are
- Evon Collett (she/her) - Manager, Partner Help Desk
- Reena Yap (she/her) - Senior Partner Help Desk Specialist, APAC focus
- Kim Stagg (they/them) - Senior Partner Help Desk Specialist, AMER/US-PubSec focus
- Yulia Imbert (she/her) - Partner Help Desk Specialist, EMEA focus

## Team Responsibilities
- Channel account record support in SFDC
- Channel account data management in Impartner
- Day-to-day partner questions via partnersupport@gitlab.com
- Partner rebate payments and tracking
- Additional responsibilities to be documented

### Out of Scope
- Channel quoting & quoting assistance (please chatter `@partner operations` on the record)
- SFDC opportunity record assistance (please chatter `@partner operations` on the record)
- Billing account set-up (please chatter the partner account owner or `@billing ops`)

## How to contact us 
The **#channel-programs-ops** Slack should be the first mode of contact for GitLab team members for all urgent and/or non-SFDC-record-related. If the request is related to an SFDC partner account record, please chatter `@partner help desk` on the record. 

Slack Best Practices  
**Please avoid contacting the Partner Help Desk team members directly via Slack.** Utlizing the #channel-programs-ops Slack channel is best to ensure timely coverage, helps others who may have similar questions, and aligns with our [Transparency value](https://about.gitlab.com/handbook/values/#transparency).

Partners can contact the Partner Support team at partnersupport@gitlab.com.

## Partner Support links & documents

<details>
<summary markdown="span">Quick Links</summary>
- [Partner Guide](https://docs.google.com/document/d/1HOzcdl22JRRbqo0SLPDsoUiM8NpNkf6-L2eiUHr6HZ4/edit)
- [Gitlab Partner Portal](https://partners.gitlab.com/English/)
- [PHD Issue Board](https://gitlab.com/groups/gitlab-com/-/boards/4547475?label_name%5B%5D=Partner%20Help%20Desk)
- [Channels Issue Board](https://gitlab.com/groups/gitlab-com/-/boards/1508300?label_name[]=Channel)
- [Internal GitLab Channel Partner Program Discounts and Incentive Guide](https://docs.google.com/document/d/1qiT_2EsnL20c4w0hyZ_CGaJQIzj8CSCsHERoR80cwws/edit?usp=sharing)
- [Working with Partner Help Desk](https://docs.google.com/presentation/u/0/d/1tT5xcx04mlFyuftL5ECPH1VCZ0pkhW7caqnCkM7a-Ro/edit)

</details>

<details>
<summary markdown="span">Handbook Pages</summary>
- [Channel Partner Handbook](https://about.gitlab.com/handbook/resellers/)
- [Channel Operations](https://about.gitlab.com/handbook/sales/field-operations/channel-operations/)
- [Channel Programs Operations](https://about.gitlab.com/handbook/sales/channel/channel-programs-ops)
- [Deal Desk](https://about.gitlab.com/handbook/sales/field-operations/sales-operations/deal-desk/#)
- [Quoting Channel Deals](https://about.gitlab.com/handbook/sales/field-operations/sales-operations/deal-desk/#quoting-channel-deals)
</details>

<details>
<summary markdown="span">Partner Portal Asset Links (portal login required)</summary>
_These links are useful to share with authorized reseller partners who already have access to the partner portal._
- [Partner Guide](https://partners.gitlab.com/prm/English/s/assets?id=404715)
- [Online Agreement](https://partners.gitlab.com/prm/English/s/assets?id=290599)
- [Deal Registration Guide](https://partners.gitlab.com/prm/English/s/assets?id=391183)
- [Partner Locator Guide](https://partners.gitlab.com/prm/English/s/assets?id=288270)
</details>

  
_More Information to Come!_  



